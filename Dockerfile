FROM php:7.4-apache
RUN docker-php-ext-install pdo pdo_mysql

RUN apt-get update
RUN apt-get upgrade -y

# Installation de Composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Installation npm
RUN apt-get install -y nodejs npm

# Import du projet
COPY . .

# Config vhost
ENV APACHE_DOCUMENT_ROOT /var/www/html/public
COPY ./Docker-vhost.conf /etc/apache2/sites-enabled/000-default.conf

# Redémarrage de Apache pour tenir compte des modifications + modules installés
RUN a2enmod rewrite && service apache2 restart

# Lancement
RUN composer install
RUN composer update
RUN npm install
